/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react'
import { View, Platform, Text, FlatList, Dimensions } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { Loading } from '../components/Loading';
import { PokemonCard } from '../components/PokemonCard';
import { SearchInput } from '../components/SearchInput';
import { usePokemonSearch } from '../hooks/usePokemonSearch';
import { styles } from '../theme/appTheme';
import { SimplePokemon } from '../interfaces/pokemonInterfaces';

const screenWidth = Dimensions.get('window').width;

export const SearchScreen = () => {

 const {top} = useSafeAreaInsets();
 const { isFetching, simplePokemonList } = usePokemonSearch();

 const [pokemonFiltered, setPokemonFiltered] = useState<SimplePokemon[]>([]);

 const [term, setTerm] = useState('');

 useEffect(() => {
   if (term === '') {
     return setPokemonFiltered([]);
   }

   setPokemonFiltered(
     simplePokemonList.filter(pokemon => pokemon.name.includes(term))
   )
 }, [term]);


 if (isFetching) {
   return <Loading />;
 }

  return (
    <View style={{flex: 1, marginHorizontal: 20}}>
      <SearchInput
      onDebounce={(value) => setTerm(value)}
      style={{
        position: 'absolute',
        zIndex: 999,
        width: screenWidth - 40,
        top: (Platform.OS === 'ios') ? top : top,
      }} />

      <FlatList
          data={pokemonFiltered}
          keyExtractor={(pokemon) => pokemon.id}
          showsVerticalScrollIndicator={false}
          numColumns={2} //Muestra la data el columnas de 2

          //Header
          ListHeaderComponent={(
            <Text style={{
              ...styles.title,
              ...styles.globalMargin,
              paddingBottom: 10,
              marginTop: (Platform.OS === 'ios') ? top : top + 70,
            }}>{term}</Text>
          )}

          renderItem={({item}) => (<PokemonCard pokemon={item}/>)}
        />
    </View>
  );
};
