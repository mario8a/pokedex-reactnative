import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { PokemonFull } from '../interfaces/pokemonInterfaces';
import { FadeInImage } from './FadeInImage';

interface Props {
  pokemon: PokemonFull;
}

export const PokemonDetails = ({pokemon}: Props) => {
  return (
    <ScrollView style={{
      ...StyleSheet.absoluteFillObject,
    }}>

      <View style={{
        ...styles.container,
        marginTop: 370
      }}>
        <Text style={styles.title}>Types</Text>
        <View style={{flexDirection: 'row'}}>
        {
          pokemon.types.map(({type}) => (
            <Text key={type.name} style={{...styles.regularTxt, marginRight: 10}}>
              {type.name}
            </Text>
          ))
        }
        </View>
        <Text style={styles.title}>Peso</Text>
        <Text style={styles.regularTxt}>{pokemon.weight} kg</Text>
      </View>

      <View style={styles.container}>
        <Text style={styles.title}>Sprites</Text>
      </View>

      <ScrollView
        showsVerticalScrollIndicator={false}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
      >
        <FadeInImage
          uri={pokemon.sprites.front_default}
          style={styles.basicSprite}
        />
        <FadeInImage
          uri={pokemon.sprites.back_default}
          style={styles.basicSprite}
        />
        <FadeInImage
          uri={pokemon.sprites.front_shiny}
          style={styles.basicSprite}
        />
        <FadeInImage
          uri={pokemon.sprites.back_shiny}
          style={styles.basicSprite}
        />
      </ScrollView>
        {/* Habilidades */}
      <View style={styles.container}>
        <Text style={styles.title}>Base Habilities</Text>
        <View style={{flexDirection: 'row'}}>
        {
          pokemon.abilities.map(({ability}) => (
            <Text key={ability.name} style={{...styles.regularTxt, marginRight: 10}}>
              {ability.name}
            </Text>
          ))
        }
        </View>
      </View>

      <View style={styles.container}>
        <Text style={styles.title}>Moves</Text>
        <View style={{flexDirection: 'row',flexWrap: 'wrap'}}>
        {
          pokemon.moves.map(({move}) => (
            <Text key={move.name} style={{...styles.regularTxt, marginRight: 10}}>
              {move.name}
            </Text>
          ))
        }
        </View>
      </View>


      <View style={styles.container}>
        <Text style={styles.title}>Stats</Text>
        <View>
        {
          pokemon.stats.map((stat, i) => (
            <View key={stat.stat.name + i} style={{flexDirection: 'row'}}>
              <Text style={{...styles.regularTxt, marginRight: 10, width: 150}}>
                {stat.stat.name}
              </Text>
              <Text style={{...styles.regularTxt, fontWeight: 'bold'}}>
                {stat.base_stat}
              </Text>
            </View>
          ))
        }
        </View>

        {/* Sprite final */}

        <View style={{
          marginBottom: 20,
          alignItems: 'center',
        }}>
          <FadeInImage
            uri={pokemon.sprites.front_default}
            style={styles.basicSprite}
          />
        </View>
      </View>

    </ScrollView>
  );
};

const styles = StyleSheet.create({
    container: {
      marginHorizontal: 20,
    },
    title: {
      marginTop: 20,
      fontWeight: 'bold',
      fontSize: 22,
    },
    regularTxt: {
      fontSize: 19,
    },
    basicSprite: {
      width: 100,
      height: 100,
    }
});
